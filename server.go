package main

import (
	"log"
	"time"

	"gitlab.com/dribbly/server/encoder"
)

// EncoderDiameter is the diameter of the encoder wheel in inches.
const EncoderDiameter = 2.0

// Pi is the value of pi we are going with for now
const Pi = 3.14159265

// DefaultCount is the count that is at the 0 point
const DefaultCount = 256

func main() {
	log.Print("Hi I'm Dribbly")
	err := encoder.Write(0x00, []byte{0x01, 0x00})
	if err != nil {
		panic(err)
	}

	data := make([]byte, 2)
	for {
		err = encoder.Read(0x00, data)
		if err != nil {
			panic(err)
		}

		var position uint16
		position = uint16(data[0]) << 8
		position |= uint16(data[1])
		distance := int32(position) - DefaultCount

		exactInches := (float32(distance) / 255.0) * EncoderDiameter * Pi
		feet := int32(exactInches) / 12
		inches := int32(exactInches) % 12

		log.Printf("%d Feet %d Inches", feet, inches)
		time.Sleep(time.Millisecond * 100)
	}
}
