package encoder

/*
	#cgo CFLAGS: -I/onion/staging_dir/target-mips_34kc_uClibc-0.9.33.2/usr/include
	#cgo LDFLAGS: -L/onion/staging_dir/target-mips_34kc_uClibc-0.9.33.2/usr/lib -loniondebug -lonionspi
	#include <onion-spi.h>
*/
import "C"
import (
	"errors"
)

// defaultSPI are the default SPI pins used to connect to the encoder.
var defaultSPI = SPI{
	SCK:  7,
	MISO: 6,
	MOSI: 1,
	SS:   0,
}

// init sets up the SPI bus.
func init() {
	err := defaultSPI.Register()
	if err != nil {
		panic(err)
	}
}

// Read reads data from the encoder
func Read(address byte, data []byte) error {
	txBytes := make([]C.uint8_t, len(data)+1)
	txBytes[0] = C.uint8_t((byte(len(data)) << 3) | (address & 0x07))

	rxBytes := make([]C.uint8_t, len(txBytes))
	err := transfer(rxBytes, txBytes)
	if err != nil {
		return err
	}

	for i := range rxBytes {
		if i == 0 {
			continue
		}
		data[i-1] = byte(rxBytes[i])
	}

	return nil
}

// Write writes data to the encoder
func Write(address byte, data []byte) error {
	txBytes := make([]C.uint8_t, len(data)+1)
	txBytes[0] = C.uint8_t(0x80 | (byte(len(data)) << 3) | (address & 0x07))
	for i := range data {
		txBytes[i+1] = C.uint8_t(data[i])
	}

	rxBytes := make([]C.uint8_t, len(txBytes))
	err := transfer(rxBytes, txBytes)
	if err != nil {
		return err
	}

	return nil
}

// transfer transfers two slices of data through the SPI bus.
func transfer(rxData, txData []C.uint8_t) error {
	if rxData == nil {
		return errors.New("SPI rxData can not be nil")
	}
	if txData == nil {
		return errors.New("SPI txData can not be nil")
	}
	if len(rxData) != len(txData) {
		return errors.New("SPI rxData and txData must have the same length")
	}

	res := C.spiTransfer(
		&defaultSPI.params,
		&txData[0],
		&rxData[0],
		C.int(len(txData)+1))
	if res != C.EXIT_SUCCESS {
		return errors.New("SPI read failed")
	}
	return nil
}

// SPI describes the pins to open an SPI bus on and can be used to register the SPI device.
type SPI struct {
	SCK  C.int
	MOSI C.int
	MISO C.int
	SS   C.int

	params C.struct_spiParams
}

// Register creates the linux SPI device.
func (spi *SPI) Register() error {
	C.spiParamInit(&spi.params)

	spi.params.sckGpio = spi.SCK
	spi.params.mosiGpio = spi.MOSI
	spi.params.misoGpio = spi.MISO
	spi.params.csGpio = spi.SS

	spi.params.mode = 0
	spi.params.bitsPerWord = 8
	spi.params.deviceId = 1
	spi.params.speedInHz = 100000

	if C.spiCheckDevice(spi.params.busNum, spi.params.deviceId, C.ONION_SEVERITY_DEBUG_EXTRA) != C.EXIT_SUCCESS {
		res := C.spiRegisterDevice(&spi.params)
		if res != C.EXIT_SUCCESS {
			return errors.New("Failed to register SPI device")
		}
	}

	res := C.spiSetupDevice(&spi.params)
	if res != C.EXIT_SUCCESS {
		return errors.New("Failed to setup SPI device")
	}

	return nil
}
